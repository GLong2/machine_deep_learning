# -*- coding: utf-8 -*-
import numpy as np

"""
    수치미분 구현 (1차 버전)
"""


def numerical_derivative(f, x):
    delta_x = 1e-4
    return (f(x+delta_x) - f(x-delta_x)) / (2 * delta_x)

"""
    ex1 -> ex1.png
"""
def my_func1(x):
    return x**2
result = numerical_derivative(my_func1, 3)
print("result == ", result)


"""
    ex2 -> ex1.png
"""
def my_func2(x):
    return 3*x*(np.exp(x))
result = numerical_derivative(my_func2, 2)
print("result == ", result)

"""
    수치미분 최종 버전
"""

def numerical_derivative(f, x):
    delta_x = 1e-4
    grad = np.zeros_like(x)

    it = np.nditer(x, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        idx = it.multi_index

        tmp_val = x[idx]
        x[idx] = float(tmp_val) + delta_x
        fx1 = f(x)

        x[idx] = tmp_val - delta_x
        fx2 = f(x)
        grad[idx] = (fx1 - fx2) / (2 * delta_x)

        x[idx] = tmp_val
        it.iternext()

    return grad
