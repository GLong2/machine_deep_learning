# -*- coding: utf-8 -*-

import numpy as np

# 1
A = np.array([
    [1, 0],
    [0, 1]
])
B = np.array([
    [1, 1],
    [1, 1]
])
print(A+B)

# 2
A = np.array([1, 2, 3])
B = np.array([4, 5, 6])
# vector A, B 출력
print("A ==", A, " B == ", B)
# vector A, B 형상 출력 => shape
print("A.shape ==", A.shape, ", B.shape ==", B.shape)
# vector A, B 차원 출력 => ndim
print("A.ndim ==", A.ndim, ", B.ndim ==", B.ndim)
# vector 산술 연산
print("A + B ==", A+B)
print("A - B ==", A-B)
print("A * B ==", A*B)
print("A / B ==", A/B)

# 3
A = np.array([[1, 2, 3], [4, 5, 6]])
B = np.array([[-1, -2, -3], [-4, -5, -6]])
# matrix A, B 형상 출력 => shape
print("A.shape ==", A.shape, ", B.shape ==", B.shape)
# matrix A, B 차원 출력 => ndim
print("A.ndim ==", A.ndim, ", B.ndim ==", B.ndim)
C = np.array([1, 2, 3])
# vector C 형상 출력 => shape
print("C.shape ==", C.shape)
# vector를 (3, 1) 행렬로 형 변환
C = C.reshape(3, 1)
print("C.shape ==", C.shape)