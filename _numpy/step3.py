# -*- coding: utf-8 -*-
import numpy as np

# 1
"""
concatenate -> 행렬에 행(row) or 열(column) 추가
머신러닝의 회귀(regression) 코드 구현 시
가중치(weight)와 바이어스(bias)를 별도로 구분하지 않고
하나의 행렬로 취급하기 위한 프로그래밍 구현 기술
"""
A = np.array([[10, 20, 30], [40, 50, 60]])
print(A.shape)

row_add = np.array([70, 80, 90]).reshape(1, 3)

column_add = np.array([1000, 2000]).reshape(2, 1)

B = np.concatenate((A, row_add), axis=0)
print(B)

C = np.concatenate((A, column_add), axis=1)
print(C)

# 2
"""
seperator로 구분된 파일에서 데이터를 읽기 위한 np.loadtxt(...)
머신러닝 코드에서 입력데이터와 정답데이터를 분리하는 프로그래밍 기법
"""
# loaded_data = np.loadtxt('file_name', delimiter=',', dtype=np.float32)
# x_data = loaded_data[:, 0: -1]
# t_data = loaded_data[:, [-1]]

# 3
"""
0 ~ 1 사이의 random number
"""
random_number1 = np.random.rand(3)
random_number2 = np.random.rand(1, 3)
random_number3 = np.random.rand(3, 1)
print("random_number1 == ", random_number1)
print("random_number2 == ", random_number2)
print("random_number3 == ", random_number3)

X = np.array([2, 4, 6, 8])
print("np.sum(X) == ", np.sum(X))
print("np.exp(X) == ", np.exp(X))
print("np.log(X) == ", np.log(X))

# 4
"""
max -> 최대값
min -> 최소값
argmax -> 최대값의 index
argmin -> 최소값의 index

axis = 0 -> 열 기준
axis = 1 -> 행 기준
"""
X = np.array([2, 4, 6 ,8])
print("np.max(X) == ", np.max(X))
print("np.min(X) == ", np.min(X))
print("np.argmax(X) == ", np.argmax(X))
print("np.argmin(X) == ", np.argmin(X))

X = np.array([[2, 4, 6], [1, 2, 3], [0, 5, 8]])
print("np.max(X) == ", np.max(X, axis=0))
print("np.min(X) == ", np.min(X, axis=0))

print("np.max(X) == ", np.max(X, axis=1))
print("np.min(X) == ", np.max(X, axis=1))

print("np.argmax(X) == ", np.argmax(X, axis=0))
print("np.argmin(X) == ", np.argmin(X, axis=0))

print("np.argmax(X) == ", np.argmax(X, axis=1))
print("np.argmin(X) == ", np.argmin(X, axis=1))

# 5
"""
ones, zeros
"""
A = np.ones([3, 3])
print("A == ", A)

B = np.zeros([3, 3])
print("B == ", B)
