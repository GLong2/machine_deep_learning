# -*- coding: utf-8 -*-
import numpy as np

# 1
A = np.array([[1, 2, 3], [4, 5, 6]])
B = np.array([[-1, -2], [-3, -4], [-5, -6]])

# 행렬 곱 dot product
C = np.dot(A, B)

print("A.shape ==", A.shape, " B.shape ==", B.shape)
print("C.shape ==", C.shape)
print(C)

# 2

A = np.array([[1, 2], [3, 4]])
# numpy broadcast // Only +, -, *, /
b = 5

print(A + b)

C = np.array([[1, 2], [3, 4]])
D = np.array([4, 5])

print(C + D)

# 3
# 전치행렬(transpose)
A = np.array([[1, 2], [3, 4], [5, 6]])
B = A.T

print("A.shape ==", A.shape, " B.shape ==", B.shape)
print(A)
print(B)

C = np.array([1, 2, 3, 4, 5])   # vector, matrix 아님
D = C.T                         # C는 vector 이므로 transpose 안됨

E = C.reshape(1, 5)             # 1x5 matrix
F = E.T                         # E의 전치행렬

print("C.shape ==", C.shape, " D.shape ==", D.shape)
print("E.shape ==", E.shape, " F.shape ==", F.shape)
print(F)

# 4
# 행렬 indexing, slicing
A = np.array([10, 20, 30, 40, 50, 60]).reshape(3, 2)
print("A.shape ==", A.shape)
print(A)

print("A[0, 0] ==", A[0, 0], ", A[0][0]==", A[0][0])
print("A[2, 1] ==", A[2, 1], ", A[2][1]==", A[2][1])

print("A[0:-1, 1:2] ==", A[0:-1, 1:2])  # [0, 1] [1, 2]를 가져옴

print("A[ :, 0] ==", A[:, 0])
print("A[ :, :] ==", A[:, :])

# 5
# 행렬 iterator
A = np.array([[10, 20, 30, 40], [50, 60, 70, 80]])
print(A, "\n")
print("A.shape ==", A.shape, "\n")
it = np.nditer(A, flags=['multi_index'], op_flags=['readwrite'])

"""
2x4 행렬인 경우,
(0, 0) => (0, 1) => (0, 2) => (0, 3) =>
(1, 0) => (1, 1) => (1, 2) => (1, 3)
"""
while not it.finished:
    idx = it.multi_index
    print("current value => ", A[idx])
    it.iternext()